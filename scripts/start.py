
import os
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
from scipy.signal import savgol_filter

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

# import seaborn as sns
plt.style.use('ggplot')
# sns.set_style("whitegrid", {'axes.grid' : False})
matplotlib.rc('animation', html='html5')
FIGSIZE = (16, 6)

ROOT = os.path.dirname(os.path.dirname(__file__))
DATA = os.path.join(ROOT, "data")
KIC4448777_data = os.path.join(DATA, "KIC4448777_data")

# PER_DAY_TO_PER_muHZ = (1 / (24 * 3600 *1e6))
PER_DAY_TO_PER_muHZ = 1e6 / (24*3600)
MAX_DAYS_FREQ = 30

def dfourier(t, y, frange, os):
    df = 1 / ((np.max(t) - np.min(t)) * os)
    loops = int((frange[1] - frange[0]) / df)

    f = np.arange(loops) * df + frange[0]
    re = np.zeros(loops)
    im = np.zeros(loops)

    for i in range(loops):
        arg = f[i] * t * 2 * np.pi
        im[i] = np.sum(y * np.sin(arg))
        re[i] = np.sum(y * np.cos(arg))

    a = 2 / len(t) * np.sqrt(re ** 2 + im ** 2)

    return f, a


def read_all_data():
    dfs = []
    files = os.listdir(KIC4448777_data)
    for datafile in files:
        datapath = os.path.join(KIC4448777_data, datafile)
        print(datafile)
        with open(datapath, "r") as f:
            lines = f.readlines()
            header = [c.strip().replace("WG 8", "WG8") for c in lines[7].strip("#\n ").rstrip("#\n ").split(",")]
            stripped_lines = [[e for e in l.strip("\n").split(" ") if e] for l in lines]
            stripped_lines = [[float(e) for e in l] for l in stripped_lines if len(l) == 7]
            df = pd.DataFrame(stripped_lines, columns=header)
            df["filename"] = datafile
            df = df[~df.isin([np.nan, np.inf, -np.inf]).any(1)]
            df['smooth'] = savgol_filter(df['WG8 Corrected Flux'], 251, 3)
            df = df.dropna()
            dfs.append(df)

    all_dfs = pd.concat(dfs)
    all_dfs["datafile_code"] = pd.factorize(all_dfs['filename'])[0] + 1

    return all_dfs


def calc_outliers_and_mean(all_data):

    all_data = all_data.sort_values('Time (days)')
    all_data['rel_flux'] = all_data['WG8 Corrected Flux'] / all_data['smooth']
    df = all_data[np.abs(all_data['rel_flux'] - all_data['rel_flux'].mean()) < 4*all_data['rel_flux'].std()]

    return df


if __name__ == '__main__':
    if not os.path.exists("astero_all_data.pkl"):
        df = read_all_data()
        df.to_pickle("astero_all_data.pkl")
    else:
        df = pd.read_pickle("astero_all_data.pkl")

    df.plot.scatter(x='Time (days)', y='WG8 Corrected Flux', c='datafile_code', s=1,
                    colormap='viridis', title="raw data", figsize=(10, 4))
    plt.savefig("../img/raw_data.png")
    plt.close()

    if not os.path.exists("astero_cleaned.pkl"):
        df = calc_outliers_and_mean(df)
        df.to_pickle("astero_cleaned.pkl")
    else:
        df = pd.read_pickle("astero_cleaned.pkl")


    df.plot.scatter(x='Time (days)', y="rel_flux", s=1,
                    c='datafile_code', colormap='viridis', title="scaled light curve", figsize=(10, 4))
    plt.savefig("../img/corrected.png")
    plt.close()

    timesteps = df['Time (days)'].values

    signal = df['rel_flux'] - df['rel_flux'].mean()
    start_time = timesteps[0]
    end_time = timesteps[-1]  # * 3600 * 24  # in seconds
    n_timesteps = len(timesteps)
    average_timestep = (end_time - start_time) / n_timesteps
    average_freq = 1. / average_timestep

    if not os.path.exists("amplitude_window.pkl"):
        freq, amp = dfourier(timesteps, signal, [0, MAX_DAYS_FREQ], 1)
        df = pd.DataFrame({'freq': freq, 'amp': amp})
        df.to_pickle("amplitude_window.pkl")
    else:
        df = pd.read_pickle("amplitude_window.pkl")
        freq = df.freq.values
        amp = df.amp.values

    fig = plt.figure(figsize=FIGSIZE)
    plt.legend('amplitude spectrum')
    plt.ylabel('amplitude spectrum [ppm]')
    plt.xlabel('Frequency (c/d)')
    plt.title('amplitude spectrum')
    plt.plot(freq, amp*1e6, linewidth=2)
    plt.savefig("../img/fft-signal-amplitudes-days.png")
    plt.close()
    # plt.show()

    fig = plt.figure(figsize=FIGSIZE)
    plt.legend('amplitude spectrum')
    plt.ylabel('amplitude spectrum [ppm]')
    plt.xlabel('Frequency ($mu Hz$)')
    plt.title('amplitude spectrum')
    plt.plot(freq * PER_DAY_TO_PER_muHZ, amp*1e6, linewidth=2)
    plt.savefig("../img/fft-signal-amplitudes-muHz.png")
    plt.close()

    fig = plt.figure(figsize=FIGSIZE)
    plt.legend('amplitude spectrum')
    plt.ylabel('amplitude spectrum [ppm]')
    plt.xlim([200, 250])
    plt.xlabel('Frequency ($mu Hz$)')
    plt.title('amplitude spectrum [200-250] ($mu Hz$)')
    plt.plot(freq * PER_DAY_TO_PER_muHZ, amp*1e6, linewidth=2)
    plt.savefig("../img/fft-signal-amplitudes-muHz-200-250.png")
    plt.close()
    np.roll.mean()
    if not os.path.exists("spectral_window.pkl"):
        freq, amp = dfourier(timesteps, [1] * timesteps, [0, MAX_DAYS_FREQ], 1)
        df = pd.DataFrame({'freq': freq, 'amp': amp})
        df.to_pickle("spectral_window.pkl")
    else:
        df = pd.read_pickle("spectral_window.pkl")
        freq = df.freq.values
        amp = df.amp.values

    fig = plt.figure(figsize=FIGSIZE)
    plt.legend('spectral window')
    plt.ylabel('spectral window')
    plt.xlabel('Frequency (c/d)')
    plt.title('spectral window ')
    plt.plot(freq, amp, linewidth=2)
    plt.savefig("../img/fft-spectral-window-days.png")
    plt.close()
    # plt.show()

    fig = plt.figure(figsize=FIGSIZE)
    plt.legend('spectral window')
    plt.ylabel('spectral window')
    plt.xlabel('Frequency (c/d)')
    plt.title('spectral window ')
    plt.plot(freq * PER_DAY_TO_PER_muHZ, amp, linewidth=2)
    plt.savefig("../img/fft-spectral-window-muHz.png")
    plt.close()
