# Asteroseismologie Data Analysis of KIC4448777

## Link

- Kallinger Data: https://ucloud.univie.ac.at/index.php/s/7xy0dezG0Oo70Sg
- Houdek Data: https://homepage.univie.ac.at/guenter.houdek/ss20/
- Mesa-Docker: https://github.com/evbauer/MESA-Docker
- Gyre: https://bitbucket.org/rhdtownsend/gyre/wiki/Running%20GYRE

## Data Processing
- remove ‘inf’ values
- remove 4σ outliers
- divide by 5d-running average
- stitch together

## Fourier Transformation


## Spectral Analysis

- fits (Lorentzian fit)

## MESA
- Install from git with as described in: https://github.com/evbauer/MESA-Docker

Install xming, and Docker. Copy files from the folder  
```
astero_mesa
``` 

into a directory under 
```bash
/home/docker/docker_work
```
E.g.
```bash
/home/docker/docker_work/mesa_dir
```

Then run the `mk` and `rn` commands in the directory.


### settings

Settings are to be done on the files
```
inlist
inlist_pgstar
inlist_project
```
#### initial_mass
initial mass in Msun units. can be any value you’d like when you are creating a pre-main sequence model.

NOTE: this is not used when loading a saved model. however is reported in output as the initial mass of the star. don’t let that confuse you.

if you are loading a ZAMS model and the requested mass is in the range of prebuilt models, the code will interpolate in mass using the closest prebuilt models. if the requested mass is beyond the range of the prebuilt models, the code will load the closest one and then call “relax mass” to create a model to match the request. the prebuilt range is 0.08 Msun to 100 Msun, so the relax_mass method is only used for extreme cases. there are enough prebuilt models that the interpolation in mass seems to work fine for many applications.

`initial_mass = 1`

#### initial_z
initial metallicity for create pre-ms and create initial model initial_z can be any value from 0 to 0.04

not used when loading a saved model. however is reported in output as the initial Z of the star.

however, if you are loading a zams model, then initial_z must match one of the prebuilt values. look in the 'data/star_data/zams_models' directory to see what prebuilt zams Z’s are available. at time of writing, only 0.02 was included in the standard version of star.
`initial_z = 0.02d0`


#### initial_y
initial helium mass fraction for create pre-ms and create initial (< 0 means use default which is `0.24 + 2*initial_z`)

not used when loading a saved model or a zams model. however is reported in output as the initial Y of the star.

NOTE: this is only used for create pre-main-sequence model and create initial model, and not when loading a zams model.

#### 


The mixing length is this parameter times a local pressure scale height. To increase R vs. L, decrease mixing_length_alpha.


#### source
the source for the star model: https://iopscience.iop.org/article/10.3847/0004-637X/817/1/65#apj521935s5

- mass: `1.02 Msun`
- metallicity: `0.022`
- helium content: `default value`
- mixing length: `1.8`

## GYRE
- in the docker, you can find gyre at: 
```bash
/home/docker/mesa/gyre
```
- make gyre: 
```bash
cd ~/mesa/gyre/gyre
make
```

- set GYRE_DIR environment variable
```bash
export GYRE_DIR=/home/docker/mesa/gyre/gyre/bin
```

- version
```
docker@70f1a938ab8a:~/docker_work/gyre_work$ /home/docker/mesa/gyre/gyre/bin/gyre --version
gyre [5.2]
```

- make folder for gyre project, which is in a mapped volume
```
/home/docker/docker_work/gyre_work
```

- move your gyre.in to your folder

- configure the created output profile in gyre.in

- Start gyre with input file: run in terminal:
```bash
/home/docker/mesa/gyre/gyre/bin/gyre gyre.in
```

- setting frequency units:

Since this is Gyre 5.2:
- to set `UHZ` frequency units: 
-- in `&scan` set:
`freq_min_units`
`freq_max_units`
-- in `&ad_output` set:
`freq_units` 
