                                                                                                                                                                                                                                                                
                        1                        2                        3
                   M_star                   R_star                   L_star
  0.2028984000000000E+034  0.2711934131737142E+012  0.2525534379709444E+035
                        1                        2                        3                        4                        5                        6                        7
                        l                      n_p                      n_g                 Re(freq)                 Im(freq)                   E_norm                        W
                        0                        3                        0  0.7125714414665026E+002  0.0000000000000000E+000  0.4034776404238433E-003  0.0000000000000000E+000
                        0                        4                        0  0.9115036746678122E+002  0.0000000000000000E+000  0.7792659323123636E-004  0.0000000000000000E+000
                        0                        5                        0  0.1102349089587870E+003  0.0000000000000000E+000  0.1636995798465928E-004  0.0000000000000000E+000
                        0                        6                        0  0.1276750715190386E+003  0.0000000000000000E+000  0.4650438215994371E-005  0.0000000000000000E+000
                        0                        7                        0  0.1448853616582122E+003  0.0000000000000000E+000  0.2012461052324487E-005  0.0000000000000000E+000
                        0                        8                        0  0.1622517448593529E+003  0.0000000000000000E+000  0.9664831351298422E-006  0.0000000000000000E+000
                        0                        9                        0  0.1790598189221025E+003  0.0000000000000000E+000  0.5786937872513436E-006  0.0000000000000000E+000
                        0                       10                        0  0.1962396097261106E+003  0.0000000000000000E+000  0.4425677768399764E-006  0.0000000000000000E+000
                        0                       11                        0  0.2137603069358901E+003  0.0000000000000000E+000  0.3519665865447115E-006  0.0000000000000000E+000
                        0                       12                        0  0.2312159649514027E+003  0.0000000000000000E+000  0.2988320806793922E-006  0.0000000000000000E+000
                        0                       13                        0  0.2488455285857200E+003  0.0000000000000000E+000  0.2781872476417191E-006  0.0000000000000000E+000
                        0                       14                        0  0.2666767888689422E+003  0.0000000000000000E+000  0.2659980962426361E-006  0.0000000000000000E+000
                        0                       15                        0  0.2845493527475354E+003  0.0000000000000000E+000  0.2580066705123491E-006  0.0000000000000000E+000
                        0                       16                        0  0.3024525071430363E+003  0.0000000000000000E+000  0.2566754352327806E-006  0.0000000000000000E+000
                        0                       17                        0  0.3203413139993445E+003  0.0000000000000000E+000  0.2598043726322716E-006  0.0000000000000000E+000
                        0                       18                        0  0.3379817144832894E+003  0.0000000000000000E+000  0.2724489009593063E-006  0.0000000000000000E+000
